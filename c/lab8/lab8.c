#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "wclist.h"

void initList(struct CListRecord *ptr, int nvalid, //initalize the list with pointers and variables
               int ntaskid,
               int nstate,
               char *ndptr,
               char ntoken,
               char ndummy,
               short nrecordid);


void printList(struct CListRecord *ptr);

int main (int argc, char **argv) {
        struct CListRecord records;
        char *cptr;
        cptr = "characters";
        for(int i = 0; i < MAX_RECORDS; i++) {
                initList(&records, i, i%5, i-5, cptr[i], 'f', 'r', i);
                printList(&records);
        }

        printf ("\nDone!\n");

        return 0;
}

void initList(struct CListRecord *base,
               int newValid,
               int newTaskid,
               int newState,
               char *newdptr,
               char newToken,
               char newDummy,
               short newRecordid
               )
{
   base->valid = newValid;
   base->taskid = newTaskid;
   base->state = newState;
   base->dptr = newdptr;
   base->token = newToken;
   base->dummy = newDummy;
   base->recordid = newRecordid;
}

void printList(struct CListRecord *base) {
      printf(" valid: %d, taskid: %d, state: %d, dptr: %c, token: %c, dummy: %c, recordid: %d\n", base->valid, base->taskid, base->state, base->dptr, base->token, base->dummy, base->recordid);
}
