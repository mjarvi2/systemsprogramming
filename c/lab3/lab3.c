#include <stdio.h>
#include<string.h>
#include<stdlib.h>

//find the average of 10 numbers, 0 is not allowed
//run "./lab3" and follow the prompts
int main(){
  int arr[10];
  float sum = 0;
  for(int i = 0; i<10; i++){
     printf("Enter value %i: ",i+1);
     scanf("%d", &arr[i]);
     if(arr[i] == 0){
	printf("incorrect input\n");
        return 0;
     }
     sum = sum + arr[i];
   }
  if(arr[9] == 0){
	printf("incorrect input\n");
        return 0;
   }
   float avg = sum/10;
   printf("Average: %f\n", avg);
   return 0;
}
