#!/bin/bash
#compile and link
echo "Script File c.sh"
#yasm -g dwarf2 -iformat=yasm -f elf64 -o asmfiles.o asmfiles.asm -l asmfiles.lst
gcc -c -S -masm=intel -g lab3.c
gcc -c -Wa,-adhln -masm=intel -g lab3.c > lab3.lst
gcc -Xlinker -Map=lab3.map -o lab3 lab3.o 
echo "Done"
