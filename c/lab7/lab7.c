#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 100

//same as lab6, but take input from file and write to one
//enter "./lab7" and it will sort the array from lab 6

int bubbleArray(int numbers[], int arraySize);
void printArray();
void swap(int arr1[], int arr2[]);
void bubbleSort(int arr[], int n);

int main(){
  char c;
  int nSize;
  int array1[SIZE];
  int retcode = SIZE;

  //populate array- only change in input from lab6
  FILE *datain = fopen("datain.txt", "r");
  for(int i = 0; i < SIZE; i++){
    fscanf(datain, "%d ", &array1[i]);
  }
  fclose(datain);
  printf("Sorting Program in Arrays: using bubble sort\n");
  printf("Unsorted Array: ");
  printArray(array1);
  bubbleSort(array1, retcode);
  printf("\nSorted Array: ");
  printArray(array1);
  //first clear file
  fclose(fopen("dataout.txt", "w"));
  //write file
  FILE *f = fopen("dataout.txt", "a");
  for(int j = 0; j<SIZE; j++){
    fprintf(f, "%d ", array1[j]);
    if(j%10 ==9){
      fprintf(f, "\n");
    }
  }
  fclose(f);
}
void bubbleSort(int arr[], int n){
  int i, j;
  for(i = 0; i < n-1; i++){
    for(j = 0; j < n-i-1; j++){
      if(arr[j] > arr[j+1]){
	swap(&arr[j], &arr[j+1]);
      }
    }
  }

}
void swap(int *arr1, int *arr2){
  int temp = *arr1;
  *arr1 = *arr2;
  *arr2 = temp;
}
void printArray(int array[]){
  for(int i = 0; i<SIZE; i++){
      printf("%d ", array[i]);
      if(i%10 == 9){
	printf("\n");
      }
  }
  printf("\n");
}
