#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 100

//make a random list of ints, then sort them using bubble sort. 
//Use "./lab6" to run

int bubbleArray(int numbers[], int arraySize);
void printArray();
void swap(int arr1[], int arr2[]);
void bubbleSort(int arr[], int n);

int main(){
  char c;
  int nSize;
  int array1[SIZE];
  int retcode = SIZE;

  //populate array
  for(int i = 0; i<SIZE; i++){
    array1[i] = rand()%1000;
  }

  printf("Sorting Program in Arrays: using bubble sort\n\n");
  printf("Unsorted Array: ");
  printArray(array1);
  bubbleSort(array1, retcode);
  printf("\nSorted Array: ");
  printArray(array1);
}
void bubbleSort(int arr[], int n){
  int i, j;
  for(i = 0; i < n-1; i++){
    for(j = 0; j < n-i-1; j++){
      if(arr[j] > arr[j+1]){
	swap(&arr[j], &arr[j+1]);
      }
    }
  }

}
void swap(int *arr1, int *arr2){
  int temp = *arr1;
  *arr1 = *arr2;
  *arr2 = temp;
}
void printArray(int array[]){
  for(int i = 0; i<SIZE; i++){
      printf("%d ", array[i]);
  }
  printf("\n");
}
