#!/bin/bash
#compile and link
echo "Script File c.sh"
#yasm -g dwarf2 -iformat=yasm -f elf64 -o asmfiles.o asmfiles.asm -l asmfiles.lst
gcc -c -S -masm=intel -g lab9.c
gcc -c -Wa,-adhln -masm=intel -g lab9.c > lab9.lst
gcc -Xlinker -Map=lab9.map -o lab9 lab9.o 
echo "Done"
