#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

unsigned int int_to_bin(unsigned int k) {
    return (k == 0 || k == 1 ? k : ((k % 2) + 10 * int_to_bin(k / 2)));
}

void multiplyNum(int input){
  int original = input;
  input = input << 2;
  printf("%d x 4 = %d\n\n",original, input);
}

void divideNum(int input){
  int original = input;
  input = input >> 2;
  printf("%d / 4 = %d\n\n",original, input);
}

void Bittest(int input){
  if(input == 1){
    printf("Passes the bit test\n\n");
  }
  else if(input == 0){
    printf("Fails the bit test\n\n");
  }
}
void Bitcount(char* input){
  int len = strlen(input);
  int counter = 0;
  for(int i = 0; i <len; i++){
    if(input[i] == '1'){
      counter = counter +1;
    }
  }
  printf("Bit Count: %d\n\n", counter);
}

void Bitor(int input, int input2){
  int result = input|input2;
  result = int_to_bin(result);
  printf("%d or %d = %d\n\n", input, input2, result);
}

void Bitand(int input, int input2){
  int result = input&input2;
  result = int_to_bin(result);
  printf("%d and %d = %d\n\n", input, input2, result);
}

void Bitxor(int input, int input2){
  int result = input^input2;
  result = int_to_bin(result);
  printf("%d xor %d = %d\n\n", input, input2, result);
}

int main(){
  int n, i;
  char* s;
  printf("enter an integer to perform multiply on: ");
  scanf("%d",&n);
  multiplyNum(n);
  printf("enter an integer to perform divide on: ");
  scanf("%d", &n);
  divideNum(n);
  printf("enter a single bit (0/1): ");
  scanf("%d", &n);
  Bittest(n);
  printf("enter a string of bits: ");
  scanf("%s", s);
  Bitcount(s);
  printf("or function \nenter number 1: ");
  scanf("%d", &n);
  printf("enter number 2: ");
  scanf("%d", &i);
  Bitor(n, i);\
  printf("and function \nenter number 1: ");
  scanf("%d", &n);
  printf("enter number 2: ");
  scanf("%d", &i);
  Bitand(n, i);
  printf("xor function \nenter number 1: ");
  scanf("%d", &n);
  printf("enter number 2: ");
  scanf("%d", &i);
  Bitxor(n, i);
  return 0;
}
