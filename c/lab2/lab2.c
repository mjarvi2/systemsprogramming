#include <stdio.h>
#include<string.h>
#include<stdlib.h>

//program to convert degrees
//Usage example "./lab2 C 30" or "./lab2 F 50"

int main(int argc,char **argv){
    if(argc !=3){
        printf("invalid input, incorrect number of arguments! \n");
        return 0;
    }
    double temp;
    temp = atof(argv[2]);
    char c = atol(argv[1]);
    
    if((strcmp(argv[1], "f") ==0)||(strcmp(argv[1], "F") == 0)){
      double temp2 = (temp-32.0)*(5.0/9.0);
      printf("Temperature in Celcius: %f\n",temp2);
      return 0;
    }
    else if((strcmp(argv[1], "c") ==0)||(strcmp(argv[1], "C") == 0)){
      double temp3 = ((temp*(9.0/5.0))+32);
      printf("Temperature in Fahrenheit: %f\n",temp3);
      return 0;
    }
    else{
      printf("invalid input, must specify Fahrenheit or Celcius \n");
      return 0;
    }
    return 0;
}
