#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//print physical location of variables and divide using long ints

int main(){
  int something;
  long somelong = 5734567345645663;
  printf("int location: %p\n", (void*)&something);
  printf("long location: %p\n", (void*)&somelong);
  long int arr[4];
  long int sum = 0;
  for(int i = 0; i<4; i++){
     printf("Enter value %i: ",i+1);
     scanf("%lu", &arr[i]);
     sum = sum + arr[i];
   }
  long int four = 4;
  ldiv_t output = ldiv(sum, four);
  int temp = 1000/four;
  int decimal  = output.rem*temp;
  if(output.rem == 1){decimal = 250;}
  if(output.rem == 2){decimal = 500;}
  if(output.rem == 3){decimal = 750;}
  // printf("quotient: %ld\n", output.quot);
  //printf("Remainder: %ld\n", output.rem);
  //printf("decimal: %f\n", decimal);
  //printf("decimal: %d", decimal);
  printf("Average: %ld.%d\n", output.quot, decimal);
}
