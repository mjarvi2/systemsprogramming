#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//Print input as strings and integers

int main (int argc, char **argv){
	printf("Name of program: %s\n", argv[0]);
	printf("Number of parameters: %d\n",argc-1);
	
	printf("Printing variables as Strings:\n");
	for(int i = 1; i< argc;i++){
	  //print the parameters as a string using %s
		printf("Parameter %d: %s\n", i, argv[i]);
	}

	printf("\nPrinting variables as Integers:\n");
	for(int j = 1; j< argc; j++){
              //print the parameters as decimals using atoi function to
              //convert from string to int
	  printf("Parameter %d: %d\n", j, atoi(argv[j]));
        }
	return 0;
}
