//-----------------------------------------------
//Michael Jarvis 
//Lab0 to illustrate some functions of ASM code 
//calab0.c
//asm0.asm
//-----------------------------------------------
#include <stdio.h> 
#include <stdlib.h> 

//C prototype 
long addnumbers(long v1, long v2);
long subnumbers(long v1, long v2); 

//two parameters by value 
//they will be passed to asm call through registers 
//  rdi, rsi

int main(int argc, char **argv) {
  long val1=0, val2=0; 
  long sum=0; 
  long diff=0;  
  val1  = strtol(argv[1], NULL, 10); 
  val2  = strtol(argv[2], NULL, 10);

    printf ("Adding and subtracting program\n\n"); 

    sum = addnumbers(val1, val2);
    diff = subnumbers(val1, val2);
    printf("%lx+%lx= %lx\n",val1,val2, sum);
    printf("%lx-%lx= %lx\n\n", val1, val2, diff);
    return 0; 
}
