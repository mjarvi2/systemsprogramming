#!/bin/bash                                                                     
#compile and link                                                               
echo "Script File c1.sh"
yasm -g dwarf2 -iformat=yasm -f elf64 -o asm1.o asm1.asm -l asm1.lst
gcc -c -S -masm=intel c1.c
gcc -c -Wa,-adhln -g -masm=intel c1.c > c1.lst
gcc -Xlinker -Map=casm1.map -o c1 c1.o asm1.o
echo "Done"
