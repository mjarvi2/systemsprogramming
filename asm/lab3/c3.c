//-----------------------------------------------
//Michael Jarvis 
//Lab0 to illustrate some functions of ASM code 
//calab0.c
//asm0.asm
//-----------------------------------------------
#include <stdio.h> 
#include <stdlib.h> 

//C prototype 
long addnumbers(long v1, long v2, long v3, long v4, long v5, long v6, long v7, long v8, long v9);
long readStack(int bottom, int top, long *sptr); 

//two parameters by value 
//they will be passed to asm call through registers 
//  rdi, rsi

int main(int argc, char **argv) {

  long *sptr;
  sptr = (long *)malloc(10*8);
  long sum=0; 
  long ssum=0;  
  long val1 = 0x19708645A345;
  long val2 = 0x1b2AE7863254;
  long val3 = 0xBEA124533512;
  long val4 = 0x178426578614;
  long val5 = 0x7816457E2345;
  long val6 = 0x892736457654;
  long val7 = 0x2716345D8943;
  long val8 = 0xC8743A896732;
  long val9 = 0xDEB894375AD;



    printf ("Adding 9 numbers by value\n"); 
    sum = addnumbers(val1, val2, val3, val4, val5, val6,val7,val8,val9);
    printf("%lx+%lx+%lx+%lx+%lx+%lx+%lx+%lx+%lx= %lx\n\n",val1,val2,val3,val4,val5,val6,val7,val8,val9, sum);
    printf("Printing up the stack\n\n");
    readStack(10,0,sptr);
    for(int i = 9; i>-1;i--){
      printf("%i: %lx\n", (i+1), sptr[i]);
    }
    printf("\nPrinting down the stack\n\n");
    for(int i = 0; i<10; i++){
      printf("%i: %lx\n", (i+1), sptr[i]);
    }
    return 0;
    
}
