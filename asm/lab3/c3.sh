#!/bin/bash                                                                     
#compile and link                                                               
echo "Script File c1.sh"
yasm -g dwarf2 -iformat=yasm -f elf64 -o asm3.o asm3.asm -l asm3.lst
gcc -c -S -masm=intel c3.c
gcc -c -Wa,-adhln -g -masm=intel c3.c > c3.lst
gcc -Xlinker -Map=casm3.map -o c3 c3.o asm3.o
echo "Done"
