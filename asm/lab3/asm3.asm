;------------------------------------------------------
; Michael Jarvis
; coded for COSC340
;
;asm1.asm
;------------------------------------------------------
section .data
;------------------------------------------------------
;-----add all variables here 
; Define standard constants.
LF    equ 10 ;line feed
NULL  equ 0 ;end of string
TRUE  equ 1
FALSE equ 0
;******************************************************
section .text
;******************************************************
global addnumbers

;-----------------------------
;add call with 6 parameters 
;  by value
;-----------------------------
addnumbers:    ;function call name 
push rbp       ;save base register
mov  rbp, rsp
mov rax, rdi
add rax, rsi
add rax, rdx
add rax, rcx
add rax, r8
add rax, r9
add rax, r10
add rax, r11
add rax, r12
addDone: ;label 
pop rbp  ;restore base register 
ret      ;end of addnumbers

global readStack

readStack:
push rbp
mov rbp, rsp

mov r10, rdi
mov r11, 0
mov rcx, rbp

rstackloop:
	cmp sil, 0
	jne upprint
	add rcx, r11
	jmp upprint
	sub rcx, r11
upprint:
	mov rax, qword[rcx]
	mov qword[rdx+r11*8], rax
	inc r11
	cmp r11, r10
	je readStackDone
	jmp rstackloop
readStackDone:
pop rbp
ret

global addStackNumber
addStackNumber:
push rbp
mov rbp, rsp





addStackDone:
pop rbp
ret

;testasm function 
;--------------------------
; function code 1 (while) 
; function code 2 (if)
; function code 3 (switch)	

global testasm

testasm:    ;function call name 
;function code in rdi
push rbp 
mov  rbp, rsp

cmp rdi, 1 ;function code  
je  forlabel
cmp rdi, 2 ;function code  
je  whilelabel
cmp rdi, 3 ;function code  
je  switchlabel
jmp endfunction

;------------------------
;loop count in rsi
forlabel:
mov rcx, rsi 
mov rax, 0
forloop: 
  inc rax 
  loop forloop
jmp endfunction 
;------------------------
;loop count in rsi
whilelabel:
mov rcx, rsi 
mov rax, 0
whileloop: 
  inc rax 
  dec rcx 
  cmp rcx,0
  je endfunction
  jmp whileloop
;------------------------
switchlabel:
;case number in rsi 
   cmp rsi, 1 
   je case1
   cmp rsi, 2 
   je case2 
   cmp rsi, 3 
   je case3 
   cmp rsi, 4 
   je case4 
   cmp rsi, 5 
   je case5 
   jmp caseerror 

 case1: 
   mov rax, 1 
   jmp endfunction 
 case2: 
   mov rax, 2
   jmp endfunction 
 case3: 
   mov rax, 3
   jmp endfunction 
 case4: 
   mov rax, 4
   jmp endfunction 
 case5: 
   mov rax, 5
   jmp endfunction 
 caseerror: 
   mov rax, -1
;------------------------
  
endfunction:
pop  rbp
