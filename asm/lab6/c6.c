#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
//assembly call prototypes

unsigned char divide64u(unsigned long d1, unsigned long d2,
                        unsigned long dv, unsigned long *qptr,
                        unsigned long *rmptr);
unsigned char divide64s(long sd1, long sd2, long sdv, long *qptrs, long *rmptrs);
bool isNumber(char number[])
{
    int i = 0;

    //checking for negative numbers
    if (number[0] == '-')
        i = 1;
    for (; number[i] != 0; i++)
    {
        //if (number[i] > '9' || number[i] < '0')
        if (!isdigit(number[i]))
            return false;
    }
    return true;
};
int main(int argc, char **argv)
{
 unsigned long values[3];
    printf("Enter Number\n");
    for (int i = 1; i <= 3; i++)
    {
        while (1 == 1)
        {
            char temp[50];
            printf("#%d: ", i);
            scanf("%s", temp);
            if (isNumber(temp))
            {
                values[i - 1] = (unsigned long)atol(temp);
                break;
            }
            else
            {
                printf("Error: Input was not a number! Try Again.\n");
            }
        }
    }
    printf("\nUnsigned Divide\n");
    char *cptr;
    int i = 0;
    cptr = (char *)malloc(50); //50 bytes
    for (i = 0; i < 50; i++)
        cptr[i] = 0x30 + i;
   
    unsigned long *qptr = (unsigned long *)malloc(sizeof(unsigned long));
    unsigned long *rmptr = (unsigned long *)malloc(sizeof(unsigned long));
    unsigned char test = divide64u(values[0],values[1],values[2],qptr,rmptr);
    unsigned long high = (unsigned long)*qptr;
    unsigned long low = (unsigned long)*rmptr;
    printf("%ld", high);
    printf(" Remainder: %ld\n", low);
    printf("\nSigned Divide\n");

    long *qptrs = (long *)malloc(sizeof(long));
    long *rmptrs = (long *)malloc(sizeof(long));
    unsigned char tests = divide64s(values[0],values[1],values[2],qptrs,rmptrs);
    long highs = (long)*qptrs;
    long lows = (long)*rmptrs;
    printf("%ld", highs);
    printf(" Remainder: %ld\n", lows);
}
