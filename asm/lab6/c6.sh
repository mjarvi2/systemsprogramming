#!/bin/bash                                                                     
#compile and link                                                               
echo "Script File c1.sh"
yasm -g dwarf2 -iformat=yasm -f elf64 -o asm6.o asm6.asm -l asm6.lst
gcc -c -S -masm=intel c6.c
gcc -c -Wa,-adhln -g -masm=intel c6.c > c6.lst
gcc -Xlinker -Map=casm6.map -o c6 c6.o asm6.o
echo "Done"
