;------------------------------------------------------
; Michael Jarvis
; coded for COSC310 
;
;------------------------------------------------------
section .data
;------------------------------------------------------
;-----add all variables here 
; Define standard constants.
LF    equ 10 ;line feed
NULL  equ 0 ;end of string
TRUE  equ 1
FALSE equ 0
;******************************************************
section .text
;******************************************************
global addnumbers9Ref
global readStack
global readRegs
;-----------------------------
;add call
;-----------------------------
addnumbers9Ref:
push rbp       ;save base register
mov  rbp, rsp


mov rax, qword[rdi]
add rax, rsi
add rax, rdx
add rax, rcx
add rax, r8
add rax, qword[r9]
add rax, qword[rbp+16]
push r10
add rax, qword[r10]
pop r10
mov r11, qword[rbp+24]
push r11
add rax, qword[r11]
pop r11
mov r12, qword[rbp+32]
push r12
add rax, qword[r12]
pop r12

addnumbers9RefDone:
pop rbp ;restore base register 
ret

global multiply64u
multiply64u:
push rbp
mov rbp,rsp
mov r8, rdx
mov rax,rdi
mul rsi
mov [r8],rdx
mov [rcx],rax
pop rbp ;restore base register 
ret
;fourth rcx

global divide64s
divide64s:
push rbp
mov rbp, rsp
mov QWORD [rbp-8], rdi
mov QWORD [rbp-16], rsi
mov QWORD [rbp-24], rdx
mov QWORD [rbp-32], rcx
mov QWORD [rbp-40], r8
mov rax, QWORD [rbp-8]
imul rax, QWORD [rbp-16]
cqo
idiv QWORD [rbp-24]
mov rdx, rax
mov rax, QWORD [rbp-32]
mov QWORD [rax], rdx
mov rax, QWORD [rbp-8]
imul rax, QWORD [rbp-16]
cqo
idiv QWORD [rbp-24]
mov  rax, QWORD [rbp-40]
mov  QWORD [rax], rdx
mov  eax, 0
pop  rbp
ret

global multiply
multiply:
push rbp ;save base register
mov rbp, rsp
mov rax, rdi
imul  rax, rsi
pop rbp   ;restore base register
ret

global divide64u
divide64u:
push rbp
mov rbp, rsp
mov QWORD [rbp-8], rdi
mov QWORD [rbp-16], rsi
mov QWORD [rbp-24], rdx
mov QWORD [rbp-32], rcx
mov QWORD [rbp-40], r8
mov rax, QWORD [rbp-8]
imul rax, QWORD [rbp-16]
mov edx, 0
div QWORD [rbp-24]
mov rdx, rax
mov rax, QWORD [rbp-32]
mov QWORD [rax], rdx
mov rax, QWORD [rbp-8]
imul rax, QWORD [rbp-16]
mov edx, 0
;div QWORD [rbp-24]
mov rax, QWORD [rbp-40]
mov QWORD [rax], rdx
mov eax, 0
pop rbp
ret

;-----------------------------
;Stack Call
;-----------------------------
readStack:
push rbp ;save base register
mov rbp, rsp

;parameters passed
;first rdi
;second rsi
;return value in rax

mov r10, rdi
mov r11, rsi
mov r12, rdx
mov rcx, r10

cmp r11, 0
je setUp
jmp setDown

setUp:
mov r13, rsp

loopUp
mov [r12], qword r13
add r12, 8
add r13, 8
loop loopUp
jmp endLoopUp

setDown:
mov r13, rbp

loopDown
mov [r12], qword r13
add r12, 8
sub r13, 8
loop loopDown

endLoopUp:

readStackDone:
pop rbp   ;restore base register
ret

;-----------------------------
;readRegs Call
;-----------------------------
readRegs:
push rbp ;save base register
mov rbp, rsp

mov r10, rdi

mov [r10], rax
add r10, 8

mov [r10], rbx
add r10, 8

mov [r10], rcx
add r10, 8

mov [r10], rdx
add r10, 8

mov [r10], rsi
add r10, 8

mov [r10], rdi
add r10, 8

mov [r10], rbp
add r10, 8

mov [r10], rsp
add r10, 8

mov [r10], r8
add r10, 8

mov [r10], r9
add r10, 8

mov [r10], r10
add r10, 8

mov [r10], r11
add r10, 8

pushfq ;Push register to stack
pop rax ;pop back to rax 
mov [r10], rax ;mov rax to r10

readRegsDone:
pop rbp ;restore base register
ret

