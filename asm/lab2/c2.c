//-----------------------------------------------
//Michael Jarvis 
//Lab0 to illustrate some functions of ASM code 
//calab0.c
//asm0.asm
//-----------------------------------------------
#include <stdio.h> 
#include <stdlib.h> 

//C prototype 
long addnumbers(long v1, long v2, long v3, long v4, long v5, long v6);
long addPNumber(long v1, long v2, long v3, long v4, long v7, long v8); 

//two parameters by value 
//they will be passed to asm call through registers 
//  rdi, rsi

int main(int argc, char **argv) {
  long val1=0, val2=0, val3=0, val4=0, val5=0, val6=0;
  long val7 = 0x0199999999999999;
  long val8  = 0x0288888888888888;
  long sum=0; 
  long psum=0;  
  val1  = strtol(argv[1], NULL, 10); 
  val2  = strtol(argv[2], NULL, 10);
  val3 = strtol(argv[3], NULL, 10);
  val4 = strtol(argv[4], NULL, 10);
  val5 = strtol(argv[5], NULL, 10);
  val6 = strtol(argv[6], NULL, 10);

    printf ("Adding 6 numbers by value\n"); 
    sum = addnumbers(val1, val2, val3, val4, val5, val6);
    psum= addPNumber(val1, val2, val3, val4, val7, val8);
    printf("%lu+%lu+%lu+%lu+%lu+%lu= %lu\n\n",val1,val2,val3,val4,val5,val6, sum);
    printf("Adding 4 numbers by value and 2 numbers by reference\n");
    printf("%lu+%lu+%lu+%lu+%lu+%lu= %lu\n",val1,val2,val3,val4,val7,val8, psum);
    return 0; 
}
