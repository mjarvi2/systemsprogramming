#!/bin/bash                                                                     
#compile and link                                                               
echo "Script File c1.sh"
yasm -g dwarf2 -iformat=yasm -f elf64 -o asm2.o asm2.asm -l asm2.lst
gcc -c -S -masm=intel c2.c
gcc -c -Wa,-adhln -g -masm=intel c2.c > c2.lst
gcc -Xlinker -Map=casm2.map -o c2 c2.o asm2.o
echo "Done"
