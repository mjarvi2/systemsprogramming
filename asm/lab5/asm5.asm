;------------------------------------------------------
;Michael Jarvis 
; coded for COSC310 
;
;asm1.asm
;------------------------------------------------------
section .data
;------------------------------------------------------
;-----add all variables here 
; Define standard constants.
LF    equ 10 ;line feed
NULL  equ 0 ;end of string
TRUE  equ 1
FALSE equ 0
;******************************************************
section .text
;******************************************************
global multiply64u

;-----------------------------
;add call with 6 parameters 
;  by value
;-----------------------------
multiply64u:    ;function call name 
push rbp       ;save base register
mov  rbp, rsp

mov rax, rdi
imul rax, rsi

jmp endfunction

endfunction: ;label 
pop rbp  ;restore base register 
ret      ;end of addnumbers
;--------------------------

