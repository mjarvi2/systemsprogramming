//-----------------------------------------------
//Michael Jarvis 
//Lab0 to illustrate some functions of ASM code 
//calab0.c
//asm0.asm
//-----------------------------------------------
#include <stdio.h> 
#include <stdlib.h> 

//C prototype 
long multiply64u(long v1, long v2); 

//six parameters by value 
//they will be passed to asm call through registers 
//  rdi, rsi, rdx, rcx, r8, r9

int main(int argc, char **argv) 
{
    long val1=0, val2=0; 
    long result=0; 
    int i=0; 
    char *cptr; 

    cptr = (char *)malloc (50); //50 bytes  
    for (i=0; i<50; i++) 
      cptr[i] = 0x30+i; 
      

    val1  = 0x094898492389499; 
    val2  = 0x023849498934923; 

    result = multiply64u(val1, val2); 
    printf("Multiplying signed longs");
    printf("%ld * %ld = %ld\n\n",val1,val2, result);

    return 0; 
}
