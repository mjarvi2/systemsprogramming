#!/bin/bash                                                                     
#compile and link                                                               
echo "Script File c1.sh"
yasm -g dwarf2 -iformat=yasm -f elf64 -o asm5.o asm5.asm -l asm5.lst
gcc -c -S -masm=intel c5.c
gcc -c -Wa,-adhln -g -masm=intel c5.c > c5.lst
gcc -Xlinker -Map=casm5.map -o c5 c5.o asm5.o
echo "Done"
