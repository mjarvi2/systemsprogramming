section .data

LF equ 10
NULL equ 0
TRUE equ 1
FALSE equ 0

section .text

global multiply64u

multiply64u:
push rbp
mov rbp, rsp

mov r9, rdx
mov r10, rcx
mov rax, rdi
mul rsi
mov qword [r9], rdx
mov qword [r10], rax

jmp endfunction

endfunction:
pop rbp
ret
