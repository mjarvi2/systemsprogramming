#!/bin/bash                                                                     
#compile and link                                                               
echo "Script File c1.sh"
yasm -g dwarf2 -iformat=yasm -f elf64 -o asm4.o asm4.asm -l asm4.lst
gcc -c -S -masm=intel c4.c
gcc -c -Wa,-adhln -g -masm=intel c4.c > c4.lst
gcc -Xlinker -Map=casm4.map -o c4 c4.o asm4.o
echo "Done"
