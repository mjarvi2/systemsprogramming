#include <stdio.h>
#include <stdlib.h>
long redRegs(long *rptr);
unsigned char multiply64u (unsigned long m1, unsigned long m2, unsigned long *highptr, unsigned long *lowptr);

int main(){
  unsigned long num1 = 0x4e35a672b;
  unsigned long num2 = 0xab745209c;
  unsigned long *highptr;
  unsigned long *lowptr;
  highptr = (long*) malloc(100);
  lowptr = (long*) malloc(100);
  multiply64u(num1, num2, highptr, lowptr);
  printf("%lx * %lx = %lx%lx\n", num1, num2, *highptr, *lowptr);
  
}
